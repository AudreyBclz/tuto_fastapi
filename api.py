import uvicorn
from fastapi import FastAPI, Form

from typing import Optional

from pydantic import BaseModel

app = FastAPI()

# get, post, put, delete, patch etc

# get
"""
    Pour lancer l'api en local on install uvicorn avec un pip install uvicorn
    et on exécute la commande uvicorn api:app --reload
"""


@app.get("/")
async def hello_world():
    return {"hello ": "world"}


@app.get("/component/{component_id}")  # path parameter
async def get_component(component_id: int):
    return {"component_id": component_id}


@app.get("/component/")
async def read_component(number: int, text: Optional[str]):
    return {"number": number,
            "text": text
            }


# On mettra http://127.0.0.1:8000/component/?number=12&text=hello word Pour l'optional On mettra
# http://127.0.0.1:8000/component/?number=12&text=  (on doit quand même spécifier le nom de la variable)

""" if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8000, reload=True)"""


# Alternative à l'install et la ligne de commande mais doit reload à chaque fois
# Lorsque l'api est lancée on a une doc générée automatiquement dans http://127.0.0.1:8000/docs


# On va créer une classe qui va hériter de BaseModel
class CoordIn(BaseModel):
    password: str
    lat: float
    lon: float
    zoom: Optional[int] = None
    description: Optional[str]


class CoordOut(BaseModel):
    lat: float
    lon: float
    zoom: Optional[int] = None
    description: Optional[str]


@app.post("/position/", response_model=CoordOut, response_model_include={'description', 'lat'})
# Ici le response_model sert à spécifier la classe de réponse  ici CoordOut
# Le response_model_include marque ce que l'on inclut seulement
# Ici on aura que la description dans la réponse
async def make_position(coord: CoordIn):
    #  db write completed
    return coord


# ---------- GERER LES FORM -----------
# on install python-multipart   pip install python-multipart
# on importe Form dans fastAPI (voir tout début)

@app.post("/login/")
async def login(username: str = Form(...), password: str = Form(...)):
    return {"username": username}

# ---------------------------------------


