# pour utiliser la création de données de manière asynchrone
# on installe databases et implicitement sqlachemy
# pip install databases

from typing import List
from fastapi import FastAPI, Depends, File
import databases
from sqlalchemy import create_engine, select, Column, LargeBinary, String, Integer
import sqlalchemy
from sqlalchemy.orm import declarative_base,sessionmaker
from datetime import datetime

DATABASE_URL = "postgresql://postgres:postgrespw@localhost:32768/test_blob"

database = databases.Database(DATABASE_URL)

Base=declarative_base()

class Files(Base):
    __tablename__='file'
    id=Column(Integer, primary_key=True)
    name = Column(String)
    blob_data = Column(LargeBinary)
    date_added= Column(sqlalchemy.DateTime)

engine = create_engine(DATABASE_URL)

Base.metadata.create_all(engine)

Session = sessionmaker(bind=engine)
session = Session()

app = FastAPI()

def add_file():
    data_path='file_send.txt'
    with open(data_path,'rb') as file:
        file_content=file.read()
    new_file=Files(name='Fichier1',blob_data=file_content,date_added=datetime.today())
    session.add(new_file)
    session.commit()
    print("fichier ajouté")

@app.on_event("startup")
async def connect():
    await database.connect()


@app.on_event("shutdown")
async def disconnect():
   await database.disconnect()

def update_file():
    retrieved_model = session.query(Files).filter_by(name='Fichier1').first()
    if retrieved_model:
        retrieved_data = retrieved_model.blob_data
        print("Retrieved data:", retrieved_data)
        data_path='file_send.txt'
        with open(data_path,'rb') as file:
            file_content=file.write()
    else:
        print("No data found.")

update_file()
# add_file()

# class Register(BaseModel):
#     id: int
#     name: str
#     date_created: datetime
#
#
# class RegisterIn(BaseModel):
#     name: str = Field(...)
#
#
# @app.post("/register/", response_model=Register)
# async def create(r: RegisterIn = Depends()):
#     query = register.insert().values(
#         name=r.name,
#         date_created=datetime.utcnow()
#     )
#     record_id = await  database.execute(query)
#     query = register.select().where(register.c.id == record_id)
#     row = await database.fetch_one(query)
#     return {**row}
#
#
# @app.get("/register/{id}", response_model=Register)
# async def get_one(id: int):
#     query = register.select().where(register.c.id == id)
#     user = await database.fetch_one(query)
#     return {**user}
#
#
# @app.get("/register", response_model=List[Register])
# async def get_all():
#     query = register.select()
#     userlist = await database.fetch_all(query)
#     return userlist
#
#
# @app.put("/register/{id}", response_model=Register)
# async def update(id: int, r: RegisterIn = Depends()):
#     query = register.update().where(register.c.id == id).values(
#         name=r.name,
#         date_created=datetime.utcnow(),
#     )
#     record_id = await database.execute(query)
#     query = register.select().where(register.c.id == record_id)
#     row = await database.fetch_one(query)
#     return {**row}
#
#
# @app.delete("/register/{id}", response_model=str)
# async def delete(id: int):
#     query = register.delete().where(register.c.id == id)
#     await database.execute(query)
#     return "ok bien supprimé"
